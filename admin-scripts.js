jQuery(document).ready(function () {
  jQuery('.editableform .editable-checklist > div > label > span').each(function (index) {
    var labelText = jQuery(this).html();
    var labelDecoded = $('<textarea />').html(labelText).text();
    jQuery(this).html(labelDecoded);
  });
});
