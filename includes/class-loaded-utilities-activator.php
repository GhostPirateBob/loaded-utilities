<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.loadedcommunications.com.au
 * @since      1.0.0
 *
 * @package    Loaded_Utilities
 * @subpackage Loaded_Utilities/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Loaded_Utilities
 * @subpackage Loaded_Utilities/includes
 * @author     Loaded Communications <webmaster@loadedcommunications.com.au>
 */
class Loaded_Utilities_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
