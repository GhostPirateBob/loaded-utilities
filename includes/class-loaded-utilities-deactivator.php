<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.loadedcommunications.com.au
 * @since      1.0.0
 *
 * @package    Loaded_Utilities
 * @subpackage Loaded_Utilities/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Loaded_Utilities
 * @subpackage Loaded_Utilities/includes
 * @author     Loaded Communications <webmaster@loadedcommunications.com.au>
 */
class Loaded_Utilities_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
