<?php

/**
 *
 * @link              https://www.loadedcommunications.com.au
 * @since             1.0.4
 * @package           Loaded_Utilities
 *
 * @wordpress-plugin
 * Plugin Name:       Loaded Utilities
 * Plugin URI:        https://www.loadedcommunications.com.au
 * Description:       Collection of utility functions and admin customisation for Loaded websites.
 * Version:           1.0.4
 * Author:            Loaded Communications
 * Author URI:        https://www.loadedcommunications.com.au
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       loaded-utilities
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Currently plugin version.
 */
define('PLUGIN_NAME_VERSION', '1.0.4');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-loaded-utilities-activator.php
 */
function activate_loaded_utilities()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-loaded-utilities-activator.php';
    Loaded_Utilities_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-loaded-utilities-deactivator.php
 */
function deactivate_loaded_utilities()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-loaded-utilities-deactivator.php';
    Loaded_Utilities_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_loaded_utilities');
register_deactivation_hook(__FILE__, 'deactivate_loaded_utilities');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-loaded-utilities.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.4
 */

// Enable ACF Options Page

if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}

// Limit Login Attempts

if (!class_exists('Limit_Login_Attempts')) {
    class Limit_Login_Attempts
    {
        public $failed_login_limit = 5;
        public $lockout_duration = 1800;
        public $transient_name = 'attempted_login';

        public function __construct()
        {
            add_filter('authenticate', array($this, 'check_attempted_login'), 30, 3);
            add_action('wp_login_failed', array($this, 'login_failed'), 10, 1);
        }

        /**
         * Lock login attempts of failed login limit is reached
         */
        public function check_attempted_login($user, $username, $password)
        {
            if (get_transient($this->transient_name)) {
                $datas = get_transient($this->transient_name);
                if ($datas['tried'] >= $this->failed_login_limit) {
                    $until = get_option('_transient_timeout_' . $this->transient_name);
                    $time = $this->when($until);
                    return new WP_Error('too_many_tried', sprintf(__('<strong>Error</strong>: Too many login attempts. Connection to database refused.'), $time));
                }
            }
            return $user;
        }

        /**
         * Add transient
         */
        public function login_failed($username)
        {
            if (get_transient($this->transient_name)) {
                $datas = get_transient($this->transient_name);
                $datas['tried']++;
                if ($datas['tried'] <= $this->failed_login_limit) {
                    set_transient($this->transient_name, $datas, $this->lockout_duration);
                }

            } else {
                $datas = array(
                    'tried' => 1,
                );
                set_transient($this->transient_name, $datas, $this->lockout_duration);
            }
        }

        /**
         * Return difference between 2 given dates
         * @param  int      $time   Date as Unix timestamp
         * @return string           Return string
         */

        private function when($time)
        {
            if (!$time) {
                return;
            }

            $right_now = time();
            $diff = abs($right_now - $time);
            $second = 1;
            $minute = $second * 60;
            $hour = $minute * 60;
            $day = $hour * 24;
            if ($diff < $minute) {
                return floor($diff / $second) . ' saniye';
            }

            if ($diff < $minute * 2) {
                return "yaklasik 1 dakika once";
            }

            if ($diff < $hour) {
                return floor($diff / $minute) . ' dakika';
            }

            if ($diff < $hour * 2) {
                return 'yaklasik  1 saat once';
            }

            return floor($diff / $hour) . ' saat';
        }
    }
}
new Limit_Login_Attempts();

// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support()
{
    $post_types = get_post_types();
    foreach ($post_types as $post_type) {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
}
add_action('admin_init', 'df_disable_comments_post_types_support');
// Close comments on the front-end
function df_disable_comments_status()
{
    return false;
}
add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);
// Hide existing comments
function df_disable_comments_hide_existing_comments($comments)
{
    $comments = array();
    return $comments;
}
add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);
// Remove comments page in menu
function df_disable_comments_admin_menu()
{
    remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'df_disable_comments_admin_menu');
// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect()
{
    global $pagenow;
    if ($pagenow === 'edit-comments.php') {
        wp_redirect(admin_url());exit;
    }
}
add_action('admin_init', 'df_disable_comments_admin_menu_redirect');
// Remove comments metabox from dashboard
function df_disable_comments_dashboard()
{
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'df_disable_comments_dashboard');
// Remove comments links from admin bar
function df_disable_comments_admin_bar()
{
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
}
add_action('init', 'df_disable_comments_admin_bar');

class loaded_color_schemes
{

    private $colors = array(
        'primary', 'flat',
    );

    public function __construct()
    {
        add_action('admin_init', array($this, 'add_colors'));
    }

    /**
     * Register color schemes.
     */
    public function add_colors()
    {
        $suffix = is_rtl() ? '-rtl' : '';

        wp_admin_css_color(
            'primary', __('Primary', 'admin_schemes'),
            plugins_url("primary/colors$suffix.css", __FILE__),
            array('#282b48', '#35395c', '#f38135', '#e7c03a'),
            array('base' => '#f1f2f3', 'focus' => '#fff', 'current' => '#fff')
        );

        wp_admin_css_color(
            'flat', __('Flat', 'admin_schemes'),
            plugins_url("flat/colors$suffix.css", __FILE__),
            array('#1F2C39', '#2c3e50', '#1abc9c', '#f39c12'),
            array('base' => '#f1f2f3', 'focus' => '#fff', 'current' => '#fff')
        );
    }
}
global $acs_colors;
$acs_colors = new loaded_color_schemes;

// Add inline CSS in the admin head with the style tag
function loaded_util_admin_head()
{
    $dir = plugin_dir_path(__FILE__);
    $admin_css = file_get_contents($dir . 'admin-style.css');
    echo '<style type="text/css">' . $admin_css . '</style>';
    $admin_js = file_get_contents($dir . 'admin-scripts.js');
    echo '<script>' . $admin_js . '</script>';
}
add_action('admin_head', 'loaded_util_admin_head');

// Add inline JS in the admin head with the <script> tag

/*
function loaded_util_admin_head() {
echo '<script type=""text/javascript">console.log('admin script')</script>';
}
add_action( 'admin_head', 'loaded_util_admin_head' );*/

function set_default_admin_color($user_id) {
    $args = array(
        'ID' => $user_id,
        'admin_color' => 'flat'
    );
    wp_update_user( $args );
}
add_action('user_register', 'set_default_admin_color');

add_filter('medium-editor-theme', 'medium_editor_color_scheme');
function medium_editor_color_scheme($theme) {
  $theme = 'beagle';
  return $theme;
}

class WP_HTML_Compression
{
    // Settings
    protected $compress_css = true;
    protected $compress_js = true;
    protected $info_comment = true;
    protected $remove_comments = true;

    // Variables
    protected $html;
    public function __construct($html)
    {
      if (!empty($html))
      {
        $this->parseHTML($html);
      }
    }
    public function __toString()
    {
      return $this->html;
    }
    protected function minifyHTML($html)
    {
      $pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
      preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
      $overriding = false;
      $raw_tag = false;
      // Variable reused for output
      $html = '';
      foreach ($matches as $token)
      {
        $tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;
        
        $content = $token[0];
        
        if (is_null($tag))
        {
          if ( !empty($token['script']) )
          {
            $strip = $this->compress_js;
          }
          else if ( !empty($token['style']) )
          {
            $strip = $this->compress_css;
          }
          else if ($content == '<!--wp-html-compression no compression-->')
          {
            $overriding = !$overriding;
            
            // Don't print the comment
            continue;
          }
          else if ($this->remove_comments)
          {
            if (!$overriding && $raw_tag != 'textarea')
            {
              // Remove any HTML comments, except MSIE conditional comments
              $content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
            }
          }
        }
        else
        {
          if ($tag == 'pre' || $tag == 'textarea')
          {
            $raw_tag = $tag;
          }
          else if ($tag == '/pre' || $tag == '/textarea')
          {
            $raw_tag = false;
          }
          else
          {
            if ($raw_tag || $overriding)
            {
              $strip = false;
            }
            else
            {
              $strip = true;
              
              // Remove any empty attributes, except:
              // action, alt, content, src
              $content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content);
              
              // Remove any space before the end of self-closing XHTML tags
              // JavaScript excluded
              $content = str_replace(' />', '/>', $content);
            }
          }
        }
        
        if ($strip)
        {
          $content = $this->removeWhiteSpace($content);
        }
        
        $html .= $content;
      }
      
      return $html;
    }
      
    public function parseHTML($html)
    {
      $this->html = $this->minifyHTML($html);
    }
    
    protected function removeWhiteSpace($str)
    {
      $str = str_replace("\t", ' ', $str);
      $str = str_replace("\n",  '', $str);
      $str = str_replace("\r",  '', $str);
      
      while (stristr($str, '  '))
      {
        $str = str_replace('  ', ' ', $str);
      }
      
      return $str;
    }
}

function wp_html_compression_finish($html)
{
    return new WP_HTML_Compression($html);
}

function wp_html_compression_start()
{
    ob_start('wp_html_compression_finish');
}
add_action('get_header', 'wp_html_compression_start');

function run_loaded_utilities()
{

    $plugin = new Loaded_Utilities();
    $plugin->run();

}
run_loaded_utilities();
