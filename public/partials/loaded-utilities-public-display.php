<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.loadedcommunications.com.au
 * @since      1.0.0
 *
 * @package    Loaded_Utilities
 * @subpackage Loaded_Utilities/public/partials
 */

